"""Stream type classes for tap-referralcandy."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_referralcandy.client import ReferralcandyStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class ContactsStream(ReferralcandyStream):
    """Define custom stream."""
    name = "contacts"
    path = "/contacts.json"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.contacts[*]"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("first_name", th.StringType),
        th.Property("last_name", th.StringType),
        th.Property("email", th.StringType),
        th.Property("purchase_made", th.BooleanType),
        th.Property("purchases", th.CustomType({"type": ["array", "string"]})),
        th.Property("unsubscribed", th.BooleanType),     
    ).to_dict()

class ReferralsStream(ReferralcandyStream):
    """Define custom stream."""
    name = "referrals"
    path = "/referrals.json"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.referrals[*]"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("referral_email", th.StringType),
        th.Property("referral_timestamp", th.StringType),
        th.Property("referring_email", th.StringType),    
        th.Property("review_period_over", th.BooleanType),    
        th.Property("external_reference_id", th.NumberType),    
    ).to_dict()
